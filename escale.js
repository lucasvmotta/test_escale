//The text file must be in the same folder of the script
filename = 'file.txt'
//The plateau, positions and instructions are declared empty, and populated by a text file called file.txt, which must be in the same folder of the application
var plateau = []
var position = undefined
var instruction = undefined
//We create a dict to manipulate the orientation. Ie: if the robot is facing NORTH, the LEFT would be WEST and the RIGHT would be EAST, and so on.
var orientation = {'N':{
						'L':'W','R':'E'},
					'W':{
						'L':'S','R':'N'},
					'S':{
						'L':'E','R':'W'},
					'E':{
						'L':'N','R':'S'},}
function main(){
	try {
		data = readFile(filename)
		move(data,plateau)
	//General exception
	} catch (err){
		console.log('Error: ',err)
	}
}

//This function is used to read the txt file
function readFile(filename){
	var fs = require('fs');
	var data = []
	//We read the input file line by line
	//If it is the first line of the file, we create a 2d array
	var text = fs.readFileSync(filename,'utf8').split('\r\n')
	for(lineIndex=0;lineIndex<text.length;lineIndex++){
		if (lineIndex == 0){
			var rows = text[lineIndex].split(' ')[0]
			var columns = text[lineIndex].split(' ')[1]
			for(x=0;x<=rows;x++){
				var line = []
				//We add a dummy value to each cell of the 2d array
				for(y=0;y<=columns;y++){
					line.push(1)
				}
				plateau.push(line)
			}
		}
		//We check if the lineIndex is divisible by 2, so we know if we are interacting with the rover position, or with rover's instructions
		else if (lineIndex % 2 != 0){
			position = text[lineIndex]
		}
		else{
			instruction = text[lineIndex]
		}
		//If we have a position and a set of instructions, we add a object to our dict
		if (position && instruction){
			data.push({'position':position,'instruction':instruction})
			//After adding to the dict, we clear the variables, so we can get the next set of instructions
			position = undefined
			instruction = undefined
		}
	}
	return data
}
//This function receives a list of data (positions and instructions), and the boundaries of the plateau
function move(data,plateau){
	for (w=0;w<data.length;w++){
		//We split the instructions into a array of characters
		splittedInstructions = data[w]['instruction'].split('')
		//We split the position and parse the integer values
		splittedPosition = data[w]['position'].split(' ')
		xPosition = parseInt(splittedPosition[0])
		yPosition = parseInt(splittedPosition[1])
		//We set the starting orientation of the robot
		facing = splittedPosition[2]
		for (x=0;x<splittedInstructions.length;x++){
			instruction = splittedInstructions[x]
			//We check if the instruction is to change the orientation of the robot, otherwise we move it
			if (['L','R'].indexOf(instruction) > -1){
				//We change the orientation based on the values of the dictionary
				facing = orientation[facing][instruction]
			} else {
				//For each movement, we check if the robot is going to move out of the plateau, if true, we dont move the robot.
				//We could throw an exception here, but since no information about this was provided in the test, I chose to keep the robot in the last valid position
				if (facing == 'N'){
					if (yPosition + 1 < plateau[0].length){
						yPosition += 1
					}
				}
				else if (facing == 'W'){
					if (xPosition - 1 >= 0){
						xPosition -= 1
					}	
				}
				else if (facing == 'S'){
					if (yPosition - 1 >= 0){
						yPosition -= 1
					}
				}
				else if (facing == 'E'){
					if (xPosition + 1 < plateau[1].length){
						xPosition += 1
					}
				}
			}
		}
		console.log(xPosition,yPosition,facing)
	}
}

//Since i am new to node.js, I decided to write some tests manually. Some libs offer a better way of doing it, but for the purpose of this test, I believe this approach is suitable.
function testInput(){
	try{
		console.log('Starting tests')
		console.log('Test: File must exist')
		var fs = require('fs');
		if (!fs.existsSync(filename)) {
		    throw new Error('File doesnt exist')
		}
		console.log('File exists: Passed')

		console.log('Test: File must contain a uneven number of lines')
		var lines = fs.readFileSync(filename,'utf8').split('\n')
		if (lines.length % 2 == 0){
			throw new Error('File has an even number of lines')
		}
		console.log("File's number of lines: Passed")

		console.log("Test: File's first line must contain two integers greater than 0")
		if (lines[0].split(' ').length != 2){
			throw new Error("File's first line of the file is wrong")
		}
		characters = lines[0].split(' ')
		for (w=0;w<characters.length;w++){
			
			if (isNaN(characters[w])){
				throw new Error("File's first line has a character that is not integer")		
			}
			else if (characters[w] < 0){
				throw new Error("File's first line has a character that is not greater than 0")
			}
		}
		console.log("File's first line: Passed")

		console.log("Test: File's uneven lines must contain two integers and one letter in [N,S,W,E]")
		for (x=0;x<lines.length;x++){
			if (x==0){
				continue
			}
			else if (x % 2 != 0){
				if (lines[x].split(' ').join('').length != 3){
					throw new Error("File's rover's position contains more than 3 characters")
				}
				if (isNaN(lines[x].split(' ')[0])){
					throw new Error("File's rover's position first character is not a number")
				}
				if (isNaN(lines[x].split(' ')[1])){
					throw new Error("File's rover's position second character is not a number")
				}
				roverInitialOrientation =  lines[x].split(' ')[2]
				if (roverInitialOrientation != 'N' && roverInitialOrientation != 'S' && roverInitialOrientation != 'W' && roverInitialOrientation != 'E'){
					throw new Error("File's rover's position third character is not in [N,S,W,E]")
				}
			}
		}
		console.log("File's uneven lines: Passed")

		console.log("Test: File's even lines must contain only letters in [M,R,L]")
		for (x=0;x<lines.length;x++){
			if (x==0){
				continue
			}
			else if (x % 2 == 0){
				splittedChars = lines[x].split('')
				for (w=0;w<splittedChars.length;w++){
					if (splittedChars[w] != 'M' && splittedChars[w] != 'R' && splittedChars[w] != 'L'){
						throw new Error("File's rover's commands are wrong")
					}
				}
			}
		}
		console.log("File's even lines: Passed")
		
		console.log('All tests passed.')

	} catch (err){
		console.log('Error: ',err)
	}
}

testInput()
main()
